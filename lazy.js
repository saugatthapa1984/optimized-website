// lazy loading images start. 

var images = document.querySelectorAll("[data-src]");

// console.log("This is the length " + images);

function preloadImage(img) {
    var src = img.getAttribute("data-src");
    if (!src) {
        return;
    }

    img.src = src;
}

var imgOptions = {
    threshold: 0,
    rootMargin: "0px 0px 200px 0px"
};

var imgObserver = new IntersectionObserver((entries, imgObserver) => {
    entries.forEach(entry => {
        if (!entry.isIntersecting) {
            return;
        } else {
            preloadImage(entry.target);
            imgObserver.unobserve(entry.target);
        }
    })
}, imgOptions);

images.forEach(image => {
    imgObserver.observe(image);
})
// lazy loading images End. 


// ***********world map image start ****************

var image = document.images[1];

// console.log(image);

var bigImage = document.createElement("img");

bigImage.onload = function () {
    image.src = this.src
    image.className = "noBlur";
}


setTimeout(function () {
    bigImage.src = "images/Extracted_WorldMap.png"
}, 50)

var image = document.images[1];

// console.log(image);

var bigImage = document.createElement("img");
// console.log(main_image);
bigImage.onload = function () {
    image.src = this.src
    image.className = "noBlur";
    document.getElementById("ban").classList.remove('first-banner');
}


setTimeout(function () {
    bigImage.src = "images/Extracted_WorldMap.png"
}, 50)

// ********** world map image end **************




// ***********solution  image start ****************

var solution_image = document.images[5];

// console.log(image);

var big_Solution_Image = document.createElement("img");
// console.log(main_image);
big_Solution_Image.onload = function () {
    solution_image.src = this.src
    solution_image.className = "noBlur solution-image position-relative";
}
setTimeout(function () {
    big_Solution_Image.src = "images/Extracted_FeatureImage3.jpg"
}, 50)

// ********** solution image end **************


$(document).ready(function () {
    $('#banner').removeClass('first-banner-replace').addClass('first-banner')
});
$(document).ready(function () {
    $('#banner').addClass('noBlur')
});

$(document).ready(function () {
    $('#banner2').removeClass('second-banner-replace').addClass('second-banner')
});
$(document).ready(function () {
    $('#banner2').addClass('noBlur')
});

// ***********Main  image start ****************

var solution_image = document.images[5];

// console.log(image);

var big_Solution_Image = document.createElement("img");
// console.log(main_image);
big_Solution_Image.onload = function () {
    solution_image.src = this.src
    solution_image.className = "noBlur solution-image position-relative";
}
setTimeout(function () {
    big_Solution_Image.src = "images/Extracted_FeatureImage3.jpg"
}, 50)

// ********** Main image end **************






// function func(){
//     alert("Image is loaded");
// }